window.addEventListener('DOMContentLoaded', async () => {
    console.log('DOM content fully loaded and parsed.');

    const url = "http://localhost:8000/api/locations/";
    try {
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            console.log(data);

            const selectLocation = document.getElementById('location');
            for (let location of data.locations) {
                // const html = `<option value="${state.abbreviation}">${state.name}</option>`
                // selectState.innerHTML += html;

                // ~~ alternate solution
                const optionTag = document.createElement('option');
                const locationId = location.href.split('/')[3];
                optionTag.value = locationId;
                optionTag.innerHTML = location.name;
                selectLocation.appendChild(optionTag);
            }
            console.log(selectLocation);
        } else {
            throw new Error('Response not ok');
        }
    }   catch (error) {
        console.error('error', error);
    }

    const formTag = document.getElementById('create-conference-form');
    formTag.addEventListener('submit', async event => {
        event.preventDefault();
        // somewhat tedious manual method of creating data object from form data
        // grabs value from each form element and shoves into data object
        // const name = document.getElementById('name').value;
        // const roomCount = document.getElementById('room_count').value;
        // const city = document.getElementById('city').value;
        // const state = document.getElementById('state').value;
        // const data = {
        //     name: name,
        //     room_count: roomCount,
        //     city: city,
        //     state: state,
        // }

        const formData = new FormData(formTag);
        // const data = {};
        // formData.forEach((value, key) => dataObj[key] = value);
        const json = JSON.stringify(Object.fromEntries(formData));
        console.log(json);

        const conferenceUrl = "http://localhost:8000/api/conferences/";
        const fetchConfig = {
            method: 'POST',
            body: json,
            headers: {
                'Content-Type': 'application/json', // tell the server its JSON
            }
        }

        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            formTag.reset();
        }
    })
});
